<?php
//file: app/code/local/MagePsycho/Testmodule/controllers/IndexController.php
class DS_Linkpoint_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
	
        $this->loadLayout();
	///Zend_Debug::dump($this->getLayout()->getUpdate()->getHandles());
	$this->renderLayout();
    }
    
    public function errorAction($order_id="",$order_NR="")
    {
        
	
	$language=Mage::getSingleton('core/session')->getStoreViewLanguage();
	
	if ($language>0)
	{
	    Mage::app()->setCurrentStore($language);    
	}
	
	
	
        try
	{
	    
	    $page = Mage::getModel('cms/page')->load('failed-transaction', 'identifier');    
	}
	catch (Exception $e)
	{
	    $cmsPageData = array(
		'title' => 'Transaction Error',
		'root_template' => 'one_column',
		'meta_keywords' => 'failed transaction',
		'meta_description' => 'Failed transaction',
		'identifier' => 'failed-transaction',
		'content_heading' => 'Failed transaction',
		'stores' => array(0),//available for all store views
		'content' => "The transaction failed! "
	    );
	    Mage::getModel('cms/page')->setData($cmsPageData)->save();
	    $page = Mage::helper('cms/page')->load('failed-transaction', 'identifier');
	}
	
	
	if (isset($order_id) && isset($order_NR))
	{
	    $page_url =Mage::helper('cms/page')->getPageUrl($page->getId())."?order_id=".$order_id."&order_NR=".$order_NR;
	    Mage::log("ir order un order nr".$page_url);
	}
	else
	{
	    $page_url =Mage::helper('cms/page')->getPageUrl($page->getId());
	    Mage::log("nav order un order nr".$page_url);
	}
	
	$this->_redirectUrl($page_url);
    }
    
    
    public function closebdAction()
    {
        $db_table_transaction = 'ds_linkpoint_transaction';
	$db_table_batch = 'ds_linkpoint_batch';
	$db_table_error = 'ds_linkpoint_error';
        require_once(Mage::getBaseDir('app').'/code/community/DS/Linkpoint/Model/ima/Merchant.php');
        
	$client_ip_addr = $_SERVER['REMOTE_ADDR'];
	$ip = $_SERVER['REMOTE_ADDR'];
	
        $testing = Mage::getStoreConfig('payment/dslinkpoint/test');
	$testing_from_developers_ips = Mage::getStoreConfig('payment/dslinkpoint/test_from_developer_ips');
	$is_developer = Mage::helper('core')->isDevAllowed();
	
	$currency =Mage::getStoreConfig('firstdatasettings/general/currency');
	$merchantid =Mage::getStoreConfig('firstdatasettings/general/merchantid');
        
        
	
	
	
        if(($testing and !$testing_from_developers_ips) or ($testing_from_developers_ips and $is_developer and $testing))
	{
	    $ecomm_client_url= Mage::getStoreConfig('firstdatasettings/testing/ecomm_client_url');
	    $ecomm_server_url= Mage::getStoreConfig('firstdatasettings/testing/ecomm_server_url');
	    $password= Mage::getStoreConfig('firstdatasettings/testing/password');
	    $certfile =Mage::getBaseDir().'/media/firstdatalv/certificates/testing/'.Mage::getStoreConfig('firstdatasettings/testing/certfile');
	}
        else
	{
	    $ecomm_client_url= Mage::getStoreConfig('firstdatasettings/production/ecomm_client_url');
	    $ecomm_server_url= Mage::getStoreConfig('firstdatasettings/production/ecomm_server_url');
	    $password= Mage::getStoreConfig('firstdatasettings/production/password');
	    $certfile =Mage::getBaseDir().'/media/firstdatalv/certificates/production/'.Mage::getStoreConfig('firstdatasettings/production/certfile');
	}
        
        
        if (date("G")==0 or 1)
        {
            $merchant = new Merchant($ecomm_server_url, $certfile, $password, 1);
            $resp = $merchant->closeDay();
                
            print_r($resp);

            //RESULT: OK RESULT_CODE: 500 FLD_075: 4 FLD_076: 6 FLD_087: 40 FLD_088: 60  
            if (strstr($resp, 'RESULT:'))
            {
                $result = explode('RESULT: ', $resp);
                $result = preg_split( '/\r\n|\r|\n/', $result[1] );
                $result = $result[0];
            }
            else
            {
                $result = '';
            }

            if (strstr($resp, 'RESULT_CODE:'))
            {
                $result_code = explode('RESULT_CODE: ', $resp);
                $result_code = preg_split( '/\r\n|\r|\n/', $result_code[1] );
                $result_code = $result_code[0];
            }
            else
            {
                $result_code = '';
            }

            if (strstr($resp, 'FLD_075:'))
            {
                $count_reversal = explode('FLD_075: ', $resp);
                $count_reversal = preg_split( '/\r\n|\r|\n/', $count_reversal[1] );
                $count_reversal = $count_reversal[0];
            }
            else
            {
                $count_reversal = '';
            }

            if (strstr($resp, 'FLD_076:'))
            {
                $count_transaction = explode('FLD_076: ', $resp);
                $count_transaction = preg_split( '/\r\n|\r|\n/', $count_transaction[1] );
                $count_transaction = $count_transaction[0];
            }
            else
            {
                $count_transaction = '';
            }

            if (strstr($resp, 'FLD_087:'))
            {
                $amount_reversal = explode('FLD_087: ', $resp);
                $amount_reversal = preg_split( '/\r\n|\r|\n/', $amount_reversal[1] );
                $amount_reversal = $amount_reversal[0];
            }
            else
            {
                $amount_reversal = '';
            }

            if (strstr($resp, 'FLD_088:'))
            {
                $amount_transaction = explode('FLD_088: ', $resp);
                $amount_transaction = preg_split( '/\r\n|\r|\n/', $amount_transaction[1] );
                $amount_transaction = $amount_transaction[0];
            }
            else
            {
                $amount_transaction = '';
            }
            $resource = Mage::getSingleton('core/resource');
	    $writeConnection = $resource->getConnection('core_write');
 
            $sql = "INSERT INTO $db_table_batch  VALUES ('', '$result', '$result_code', '$count_reversal', '$count_transaction', '$amount_reversal', '$amount_transaction', now(), '$resp')";
            $writeConnection->query($sql);
        }
    }
    
    public function okAction()
    {
        $db_table_transaction = 'ds_linkpoint_transaction';
	$db_table_batch = 'ds_linkpoint_batch';
	$db_table_error = 'ds_linkpoint_error';
	
        $client_ip_addr = $_SERVER['REMOTE_ADDR'];
	$ip = $_SERVER['REMOTE_ADDR'];
        
        $testing = Mage::getStoreConfig('payment/dslinkpoint/test');
	$testing_from_developers_ips = Mage::getStoreConfig('payment/dslinkpoint/test_from_developer_ips');
	$is_developer = Mage::helper('core')->isDevAllowed();
	
	$currency =Mage::getStoreConfig('firstdatasettings/general/currency');
	$merchantid =Mage::getStoreConfig('firstdatasettings/general/merchantid');
        
        $comment ="";
        if(($testing and !$testing_from_developers_ips) or ($testing_from_developers_ips and $is_developer and $testing))
	{
	    $ecomm_client_url= Mage::getStoreConfig('firstdatasettings/testing/ecomm_client_url');
	    $ecomm_server_url= Mage::getStoreConfig('firstdatasettings/testing/ecomm_server_url');
	    $password= Mage::getStoreConfig('firstdatasettings/testing/password');
	    //$certfile =Mage::getStoreConfig('firstdatasettings/testing/certfile');
	    $certfile =Mage::getBaseDir().'/media/firstdatalv/certificates/testing/'.Mage::getStoreConfig('firstdatasettings/testing/certfile');
	    $comment.="TESTING MODE: ";
			
	}
	else
	{
	    $ecomm_client_url= Mage::getStoreConfig('firstdatasettings/production/ecomm_client_url');
	    $ecomm_server_url= Mage::getStoreConfig('firstdatasettings/production/ecomm_server_url');
	    $password= Mage::getStoreConfig('firstdatasettings/production/password');
	    $certfile =Mage::getBaseDir().'/media/firstdatalv/certificates/production/'.Mage::getStoreConfig('firstdatasettings/production/certfile');
	}
        
        
        
        $language=Mage::getSingleton('core/session')->getStoreViewLanguage();
        
        Mage::app()->setCurrentStore($language);
        
        $transaction_url =Mage::getSingleton('core/session')->getTransactionUrl();
        $failed=1;
        //$orderId = Mage::getSingleton('checkout/session')->getLastRealOrderId();
        $orderId =  Mage::getSingleton('checkout/session')->getLastOrderId();
        $orderId_NR =  Mage::getSingleton('core/session')->getPaymentOrderId();
        
        if (isset($_POST['trans_id']))
        {
            if (isset($_POST['error']))
            {
            
		$comment.=" Failed, post errror";
                
            }
            else
            {
                require_once(Mage::getBaseDir('app').'/code/community/DS/Linkpoint/Model/ima/Merchant.php');
                $trans_id = $_POST['trans_id'];
                $merchant = new Merchant($ecomm_server_url, $certfile, $password, 1);
                $resp = $merchant -> getTransResult(urlencode($trans_id), $client_ip_addr);
                
                
                Mage::log($resp);
                            
			    
		
                $txt = $resp;
                if (strstr($resp, 'RESULT:'))
                {
                    //$resp example RESULT: OK RESULT_CODE: 000 3DSECURE: NOTPARTICIPATED RRN: 915300393049 APPROVAL_CODE: 705368 CARD_NUMBER: 4***********9913 
                    if (strstr($resp, 'RESULT:'))
                    {
                        $result = explode('RESULT: ', $resp);
                        $result = preg_split( '/\r\n|\r|\n/', $result[1] );
                        $result = $result[0];
                    }
                    else
                    {
                        $result = '';
                    }
                    if (strstr($resp, 'RESULT_CODE:'))
                    {
                        $result_code = explode('RESULT_CODE: ', $resp);
                        $result_code = preg_split( '/\r\n|\r|\n/', $result_code[1] );
                        $result_code = $result_code[0];
			$comment.="We got transfare result should be ok =?= ".$result_code;
                    }
                    else
                    {
                        $result_code = '';
                    }

                    if (strstr($resp, '3DSECURE:'))
                    {
                        $result_3dsecure = explode('3DSECURE: ', $resp);
                        $result_3dsecure = preg_split( '/\r\n|\r|\n/', $result_3dsecure[1] );
                        $result_3dsecure = $result_3dsecure[0];
			$comment.="We got transfare result should be ok =?= ".$result_3dsecure;
                    }
                    else
                    {
                        $result_3dsecure = '';
                    }
                        
                    if (strstr($resp, 'CARD_NUMBER:'))
                    {
                        $card_number = explode('CARD_NUMBER: ', $resp);
                        $card_number = preg_split( '/\r\n|\r|\n/', $card_number[1] );
                        $card_number = $card_number[0];
                    }
                    else
                    {
                        $card_number = '';
                    }
                    
		    $resource = Mage::getSingleton('core/resource');
                    $writeConnection = $resource->getConnection('core_write');
                    $sql1 = "UPDATE $db_table_transaction SET 
                                    result = '$result',
                                    result_code = '$result_code',
                                    result_3dsecure = '$result_3dsecure',
                                    card_number = '$card_number',
                                    response = '$resp'      
                                    WHERE trans_id = '$trans_id'";
				    
                    $writeConnection->query($sql1);
                                
                    $sql2 = "UPDATE $db_table_transaction SET 
				    result = '$result',
                                    result_code = '$result_code',
                                    result_3dsecure = '$result_3dsecure',
                                    card_number = '$card_number',
                                    response = '$resp'
				    
                                    WHERE trans_id = '$trans_id'";
                                    
                    $writeConnection->query($sql2);
		    
		    
		    if ($result!='FAILED' and (strlen($result)>0 or strlen($card_number)>0 or strlen($result_3dsecure)>0 or strlen($result_code)>0))
		    {
			$orderId =  Mage::getSingleton('checkout/session')->getLastOrderId();
		        $order = Mage::getModel('sales/order')->load($orderId);
			
			
		        $order->addStatusToHistory($order->getStatus(), 'Payment succesfull transaction id -'.$trans_id." response: $txt", false);
			
			
			//$payment = $order->getPayment();
			//$payment->setTransactionId($trans_id);
			//$transaction = $payment->addTransaction($typename, null, false, $comment);
			//$transaction->setParentTxnId($transactionID);
			//$transaction->setIsClosed(0);
			//$transaction->setAdditionalInformation("resp", $txt);
		        //$transaction->save()
		        /*
			try {
				if(!$order->canInvoice())
				{
				    Mage::throwException(Mage::helper('core')->__('Cannot create an invoice.'));
				}
				$invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
				
				if (!$invoice->getTotalQty())
				{
				    Mage::throwException(Mage::helper('core')->__('Cannot create an invoice without products.'));
				}
				
				$invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_ONLINE);
				$invoice->register();
				$transactionSave = Mage::getModel('core/resource_transaction')
				->addObject($invoice)
				->addObject($invoice->getOrder());
				$transactionSave->save();
			}
			catch (Mage_Core_Exception $e)
			{
 
			}
			*/
			
			$order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, true,"",false);
			$order->save();
			
			
		        try {
				$order->sendNewOrderEmail();
                                //echo "Order $orderId successfully sent\n";
			} catch (Exception $e)
			{
			    //echo $e->getMessage();
			}
			$order_NR =  $order->getIncrementId();
			Mage::log("orderid, order_nr = $orderId,$order_NR");
			if ($order_NR and $orderId)
			{
			    $this->_redirect('checkout/onepage/success?order_id='.$orderId."&order_NR="$order_NR);    
			}
			else
			{
			    $this->_redirect('checkout/onepage/success');    
			}
			
			return;
		    }
		    else
		    {
			$comment.=" Failed, no transaction variable";
		    }

                }
                
            }
                
        }
        else
        {
	    $comment.=" Failed, no transaction variable";
            
        }
	$orderId =  Mage::getSingleton('checkout/session')->getLastOrderId();
	$order = Mage::getModel('sales/order')->load($orderId);
	$order->addStatusToHistory($order->getStatus(), 'This order is canceled due to failed Firsdata transaction'.$trans_id." - ".$txt, false);
        $order->setState(Mage_Sales_Model_Order::STATE_CANCELED, true)->save();
                            
        try {
                $order->sendOrderUpdateEmail(true, $this->__("YOUR ORDER CANCELED DUE TO THE FAILED TRANSACTION"));
                //echo "Order $orderId successfully sent\n";
        } catch (Exception $e) {
            //echo $e->getMessage();
        }
	
        $order_NR =  $order->getIncrementId();
	Mage::log("orderid, order_nr = $orderId,$order_NR");
	$this->errorAction($orderId,$order_NR);
	
    }
    
    
    
    public function checkpendingordersAction()
    {
        //selecting pending orders which are older thean 1 hour
        $content="";
        $time_before_one_hour = $date = date('Y-m-d H:i:s', strtotime('-4 hour'));
        
        
        
        
        
        $orders = Mage::getResourceModel('sales/order_collection')
            ->addFieldToSelect('*')
            ->addFieldToFilter('status', array('in'=> array('pending')))
            ->addFieldToFilter('updated_at',array('from'=>'0000-00-00','to'=>$time_before_one_hour,'date'=>true));    
        
        
        $order_count = $orders->count();
        
        
            
        $content.="Chekcing all orders pending orders before ".$time_before_one_hour." date, there are $order_count orders to check <br/>";
        
        
        
        print($content);
	
        if ($order_count>0)
        {
            foreach($orders as $order)
            {
                
                $_HISTORY = $order->getAllStatusHistory();
		
                $skip=1;
                foreach($_HISTORY as $h_item)
                {
                    $comment = $h_item->getData('comment');
                    
                    
                    if (substr_count($comment,"Transaction ID:")>0 and substr_count($comment,"First Data Lv")>0)
                    {
			//print($comment."<br/>");
                      $skip=0;
                    }
		    else
		    {
			//print($comment."<br/>");
		    }
                }
		
                if ($skip)
                {
                    print($order->getId()." [".$order->getRealOrderId()."] SKIPPED no transaction id so probably not an firstdata lv order<br/>");    
                }
                else
                {
		    print($order->getId()." [".$order->getRealOrderId()."] CANCELED end of waiting time firstdata lv order<br/>");    
                    $order->setState(Mage_Sales_Model_Order::STATE_CANCELED, true,"Auto cannceled order",true)->save();
		    //print('we are here');
		    //die();
                    
                }
                
            }
        }
        
    }

}