<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * which is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you are unable to obtain it through the world-wide-web,
 * please send an email to magento@raveinfosys.com
 * so we can send you a copy immediately.
 *
 * @category	DS
 * @package		DS_Linkpoint
 * @author		RaveInfosys, Inc.
 * @license		http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class DS_Linkpoint_Model_Linkpoint extends Mage_Payment_Model_Method_Cc {
	
	protected $_code		=	'dslinkpoint';	//unique internal payment method identifier
	
	protected $_isGateway					=	true;		//Is this payment method a gateway (online auth/charge) ?
	protected $_canAuthorize				=	true;			//Can authorize online?
	protected $_canCapture					=	true;		//Can capture funds online?
	protected $_canCapturePartial			=	false;			//Can capture partial amounts online?
	protected $_canRefund					=	true;		//Can refund online?
	protected $_canRefundInvoicePartial		=	true;			//Can refund invoices partially?
	protected $_canVoid						=	true;		//Can void transactions online?
	protected $_canUseInternal				=	true;			//Can use this payment method in administration panel?
	protected $_canUseCheckout				=	true;			//Can show this payment method as an option on checkout payment page?
	protected $_canUseForMultishipping		=	false;			//Is this payment method suitable for multi-shipping checkout?
	protected $_isInitializeNeeded			=	false;
	protected $_canFetchTransactionInfo		=	false;
	protected $_canReviewPayment			=	false;
	protected $_canCreateBillingAgreement	=	false;
	protected $_canManageRecurringProfiles	=	false;
	protected $_canSaveCc					=	false;			//Can save credit card information for future processing?
	
	/**
     * Fields that should be replaced in debug with '***'
     *
     * @var array
     */
    protected $_debugReplacePrivateDataKeys = array('cvmvalue', 'keyfile', 'cardnumber', 'cardexpmonth', 'cardexpyear');
	
	/**
     * Validate payment method information object
     *
     * @param   Mage_Payment_Model_Info $info
     * @return  Mage_Payment_Model_Abstract
     */
	public function validate() {
		Mage::log(__FUNCTION__);
		//we dont validate cause have to resend to first data page
		//print(__FUNCTION__);
		//parent::validate();
	}
	
	
	public function capture(Varien_Object $payment, $amount)
	{ 

		// ... You payment method authorization goes here ...
		// Here goes retrieving or generation non-zero, 
		// non-null value as transaction ID. 
		//$transactionId = $api->someCall(); 
		// Setting tranasaction id to payment object
		// It is improtant, if you want perform online actions 
		// in future with this order!
		
		
		$db_table_transaction = 'ds_linkpoint_transaction';
		$db_table_batch = 'ds_linkpoint_batch';
		$db_table_error = 'ds_linkpoint_error';
	
		
		// localCurr - "EUR"
		$baseCurr = Mage::app()->getStore()->getBaseCurrencyCode(); 

		// baseCurr - "GBP"
		$currentCurr = Mage::app()->getStore()->getCurrentCurrencyCode();
		
		$ip="";
		$client_ip_addr = $_SERVER['REMOTE_ADDR'];
		$ip = $_SERVER['REMOTE_ADDR'];
		
		$testing = Mage::getStoreConfig('payment/dslinkpoint/test');
		$testing_from_developers_ips = Mage::getStoreConfig('payment/dslinkpoint/test_from_developer_ips');
		$is_developer = Mage::helper('core')->isDevAllowed();
		
		
		$currency =Mage::getStoreConfig('firstdatasettings/general/currency');
		$merchantid =Mage::getStoreConfig('firstdatasettings/general/merchantid');
		
		$linkpoint_currency="";
		$grandTotal_InBaseCurrency="";
		$grandTotal=$amount;
		switch($currency)
		{
			case 826:
				$linkpoint_currency="GBP";
			break;
		
			case 978:
				$linkpoint_currency="EUR";
			break;
		
			case 840:
				$linkpoint_currency="USD";
			break;
			case 941:
				$linkpoint_currency="RSD";
			break;
			case 703:
				$linkpoint_currency="SKK";
			break;
			case 440:
				$linkpoint_currency="LTL";
			break;
			case 643:
				$linkpoint_currency="RUB";
			break;
		
			case 891:
				$linkpoint_currency="YUM";
			break;
		
			default:
				$currency=978;
				$linkpoint_currency="EUR";
			break;
		}
		
		if ($baseCurr==$linkpoint_currency)
		{
			//if base currency is same as in linkpoint currency
			$grandTotal = floor($amount*100);
		}
		else
		{
			//the base currency is not as in linkpoint
			if ($currentCurr==$linkpoint_currency)
			{
				//checking if current currency is same
				$grandTotal = floor($amount*100);	
			}
			else
			{
				//not the same, have to convert from base to the linkpoint
				$grandTotal_InBaseCurrency = floor($amount*100);
				$grandTotal = Mage::helper('directory')->currencyConvert($grandTotal_InBaseCurrency, $baseCurr, $currentCurr);				
			}
		}
		
		
		if(($testing and !$testing_from_developers_ips) or ($testing_from_developers_ips and $is_developer and $testing))
		{
			Mage::log("Testing Mode ".__LINE__.__FUNCTION__);
			$ecomm_client_url= Mage::getStoreConfig('firstdatasettings/testing/ecomm_client_url');
			$ecomm_server_url= Mage::getStoreConfig('firstdatasettings/testing/ecomm_server_url');
			$password= Mage::getStoreConfig('firstdatasettings/testing/password');
			//$certfile =Mage::getStoreConfig('firstdatasettings/testing/certfile');
			$certfile =Mage::getBaseDir().'/media/firstdatalv/certificates/testing/'.Mage::getStoreConfig('firstdatasettings/testing/certfile');
			
		}
		else
		{
			Mage::log("production Mode ".__LINE__.__FUNCTION__);
			$ecomm_client_url= Mage::getStoreConfig('firstdatasettings/production/ecomm_client_url');
			$ecomm_server_url= Mage::getStoreConfig('firstdatasettings/production/ecomm_server_url');
			$password= Mage::getStoreConfig('firstdatasettings/production/password');
			$certfile =Mage::getBaseDir().'/media/firstdatalv/certificates/production/'.Mage::getStoreConfig('firstdatasettings/production/certfile');
		}
		require_once(Mage::getBaseDir('app').'/code/community/DS/Linkpoint/Model/ima/Merchant.php');
		
		
		$order = $payment->getOrder();
		$BillingAddress	=$order->getBillingAddress();	
		$billing_name = $BillingAddress->getFirstname()." ".$BillingAddress->getLastname();
		$description  = urlencode('Payment from '.$billing_name.' - '.$grandTotal.' '.$linkpoint_currency);
		Mage::log($description." ".__LINE__." ".__FILE__);
		if (strlen($ip)==0)
		{
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		$language = "";
		
		
		
		
		$amount=$grandTotal;
		
		Mage::log("$ecomm_server_url, $certfile, $password, 1");
		$merchant = new Merchant($ecomm_server_url, $certfile, $password, 1);
		
	
		Mage::log("$amount, $currency, $ip, $description, $language");
		$resp = $merchant->startSMSTrans($amount, $currency, $ip, $description, $language);
		
		Mage::log($resp);
		Mage::log(" ".__LINE__." ".__FILE__);
		
		
		if (substr($resp,0,14)=="TRANSACTION_ID")
		{
			
			$trans_id = substr($resp,16,28);
			$redirect_url = $ecomm_client_url."?trans_id=". urlencode($trans_id);
			
			/**
			* Get the resource model
			*/
			$resource = Mage::getSingleton('core/resource');
			$writeConnection = $resource->getConnection('core_write');
 
	
    
     
			$query = "INSERT INTO $db_table_transaction VALUES ('', '$trans_id', '$amount', '$currency', '$ip', '$description', '$language', 'NO', '???', '???', '???', '???', now(), '$resp', '', '')";
     
			/**
			* Execute the query
			*/
			
			
			$writeConnection->query($query);
			
			$order->addStatusToHistory($order->getStatus(), "Sending User to First Data Lv Form, waiting user to fill the form and return... Transaction ID:".$trans_id, false);
			$order->save();
			
			Mage::getSingleton('core/session')->setTransactionId($trans_id);
			Mage::getSingleton('core/session')->setTransactionUrl($redirect_url);
			Mage::getSingleton('core/session')->setStoreViewLanguage(Mage::app()->getStore()->getCode());
			
			
         
		}
		else
		{
			Mage::throwException($resp);
			$redirect_url="";
		}
		$payment->setTransactionId($trans_id);
		//TODO: shoudl make the transaction not colse and not succes
		// Before user returns..
		$payment->setIsTransactionClosed(1);
		$payment->setStatus(self::STATUS_SUCCESS);
		$payment->save();
		Mage::log($trans_id.__FUNCTION__." ".__FILE__);
		// ... some other your actions ...
		Mage::log(__FUNCTION__." ".__FILE__);
		return $this;
		
		
	}
	
	
	/**
     * Send capture request to gateway
     *
     * @param Varien_Object $payment
     * @param decimal $amount
     * @return Mage_Paygate_Model_Authorizenet
     * @throws Mage_Core_Exception
     * 
     */
	/*
	public function capture(Varien_Object $payment, $amount)
	{
		Mage::log(__FUNCTION__." ".__FILE__);
		$payment->setAnetTransType(self::REQUEST_TYPE_PRIOR_AUTH_CAPTURE);
		//$trans_id = Mage::getSingleton('core/session')->getTransactionId();
		//$payment->setTransactionId($trans_id);
		//$payment->setIsTransactionClosed(1);
		//$payment->setStatus(self::STATUS_SUCCESS);
		//$payment->save();
		
		
		return $this;
	}
	*/
	
	/**
     * refund the amount with transaction id
     *
     * @param string $payment Varien_Object object
     * @return Mage_Paygate_Model_Authorizenet
     * @throws Mage_Core_Exception
     */
    public function refund(Varien_Object $payment, $amount)
    {
	Mage::log(__FUNCTION__." ".__FILE__);
	$db_table_transaction = 'ds_linkpoint_transaction';
	$db_table_batch = 'ds_linkpoint_batch';
	$db_table_error = 'ds_linkpoint_error';
        
        
        $testing = Mage::getStoreConfig('payment/dslinkpoint/test');
	$currency =Mage::getStoreConfig('firstdatasettings/general/currency');
	$merchantid =Mage::getStoreConfig('firstdatasettings/general/merchantid');
        
        $comment ="";
        if(($testing and !$testing_from_developers_ips) or ($testing_from_developers_ips and $is_developer and $testing))
	{
	    $ecomm_client_url= Mage::getStoreConfig('firstdatasettings/testing/ecomm_client_url');
	    $ecomm_server_url= Mage::getStoreConfig('firstdatasettings/testing/ecomm_server_url');
	    $password= Mage::getStoreConfig('firstdatasettings/testing/password');
	    //$certfile =Mage::getStoreConfig('firstdatasettings/testing/certfile');
	    $certfile =Mage::getBaseDir().'/media/firstdatalv/certificates/testing/'.Mage::getStoreConfig('firstdatasettings/testing/certfile');
	    $comment.="TESTING MODE: ";
			
	}
	else
	{
	    $ecomm_client_url= Mage::getStoreConfig('firstdatasettings/production/ecomm_client_url');
	    $ecomm_server_url= Mage::getStoreConfig('firstdatasettings/production/ecomm_server_url');
	    $password= Mage::getStoreConfig('firstdatasettings/production/password');
	    $certfile =Mage::getBaseDir().'/media/firstdatalv/certificates/production/'.Mage::getStoreConfig('firstdatasettings/production/certfile');
	}
	
	require_once(Mage::getBaseDir('app').'/code/community/DS/Linkpoint/Model/ima/Merchant.php');
        
        $client_ip_addr = $ip = $_SERVER['REMOTE_ADDR'];
	
	$transactionId = $payment->getRefundTransactionId();
	Mage::log("transaction id");
	Mage::log($transactionId);
	Mage::log("transaction id");
		
	
	
	
	$order =$payment->getOrder();
	$order_id  = $order->getId();

	$order = Mage::getModel("sales/order")->load($order_id);
	$order->addStatusToHistory($order->getStatus(), "Refunding the $amount", false);
	$order->save();
	
	
	if (strlen($transactionId)>0 and $amount>0)
	{
		$trans_id = $transactionId;
		$amount = $amount*100;
		$trans_id = urlencode($trans_id);
		$merchant = new Merchant($ecomm_server_url, $certfile, $password, 1);
		$resp = $merchant->reverse(str_replace(" ","",$trans_id), $amount);
		$order->addStatusToHistory($order->getStatus(), "Result of refunding $resp", false);
		$order->save();
		Mage::log($resp);
		if (substr($resp,8,2) == "OK" OR substr($resp,8,8) == "REVERSED")
		{
			if (strstr($resp, 'RESULT:'))
			{
				$result = explode('RESULT: ', $resp);
				$result = preg_split( '/\r\n|\r|\n/', $result[1] );
				$result = $result[0];
			}
			else
			{
				$result = '';
			}
			if (strstr($resp, 'RESULT_CODE:'))
			{
				$result_code = explode('RESULT_CODE: ', $resp);
				$result_code = preg_split( '/\r\n|\r|\n/', $result_code[1] );
				$result_code = $result_code[0];
			}
			else
			{
				$result_code = '';
			}
				
			if (strstr($resp, 'CARD_NUMBER:'))
			{
			        $card_number = explode('CARD_NUMBER: ', $resp);
			        $card_number = preg_split( '/\r\n|\r|\n/', $card_number[1] );
			        $card_number = $card_number[0];
			}
			else
			{
			        $card_number = '';
			}	
		}
		
		
		$resource = Mage::getSingleton('core/resource');
                $writeConnection = $resource->getConnection('core_write');
		$sql = "UPDATE $db_table_transaction SET 
				result = '$result',
                                result_code = '$result_code',
                                result_3dsecure = '???',
                                card_number = '$card_number',
                                response = '$resp'      
                                WHERE trans_id = '$trans_id'";
		$writeConnection->query($sql);
		
		$sql = "UPDATE $db_table_transaction SET 
				result = '$result',
                                result_code = '$result_code',
                                result_3dsecure = '???',
                                card_number = '$card_number',
                                response = '$resp'      
                        WHERE trans_id = '$trans_id'";
		$writeConnection->query($sql);
			
			
	}
		
	if (substr($resp,8,2) == "OK" OR substr($resp,8,8) == "REVERSED")
	{
		$payment->setStatus(self::STATUS_SUCCESS);
		return $this;
	}
	else
	{
		Mage::throwException(Mage::helper('paygate')->__('Error in refunding the payment.'));
	}
	
}
	
	/**
     * Void the payment through gateway
     *
     * @param Varien_Object $payment
     * @return Mage_Paygate_Model_Authorizenet
     * @throws Mage_Core_Exception
     */
    public function void(Varien_Object $payment)
    {
	Mage::log(__FUNCTION__." ".__FILE__);
	
    }
	
	/**
     * Cancel payment
     *
     * @param   Varien_Object $invoicePayment
     * @return  Mage_Payment_Model_Abstract
     */
    public function cancel(Varien_Object $payment) {
	Mage::log(__FUNCTION__." ".__FILE__ );
	
    }
	
	/**
     * converts a hash of name-value pairs
     * to the correct XML format for LSGS
	 *
     * @param Array $pdata
     * @return String $xml
     */
	protected function _buildRequest($pdata) {
		Mage::log(__FUNCTION__." ".__FILE__);
		
	}
	
	/**
     * converts the LSGS response xml string
     * to a hash of name-value pairs
	 *
     * @param String $xml
     * @return Array $retarr
     */
	protected function _readResponse($xml) {
		Mage::log(__FUNCTION__." ".__FILE__);
		
		
	}
	
	/**
     * process hash table or xml string table using cURL
	 *
     * @param Array $data
     * @return String $xml
     */
	protected function _postRequest($data) {
		Mage::log(__FUNCTION__." ".__FILE__);
		
		
	}
	
	protected function _prepareData() {
		
		Mage::log(__FUNCTION__." ".__FILE__);
		
	}
	
	
	public function getOrderPlaceRedirectUrl()
	{
		
		Mage::log(__FUNCTION__." ".__FILE__);
		$redirect_url = Mage::getSingleton('core/session')->getTransactionUrl();
		return $redirect_url;
	}
}
?>