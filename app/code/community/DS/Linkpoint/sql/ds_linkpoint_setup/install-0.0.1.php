<?php


$db_table_transaction   =     'ds_linkpoint_transaction';
$db_table_batch         =     'ds_linkpoint_batch';
$db_table_error         =     'ds_linkpoint_error';

$this->startSetup();
$this->run("
CREATE TABLE  $db_table_transaction  (
      id INT(10) NOT NULL AUTO_INCREMENT,
      trans_id VARCHAR(50),
      amount INT(10),
      currency INT(10),
      client_ip_addr VARCHAR(50),
      description TEXT,
      language VARCHAR(50),
      dms_ok VARCHAR(50),
      result VARCHAR(50),
      result_code VARCHAR(50),
      result_3dsecure VARCHAR(50),
      card_number VARCHAR(50),
      t_date VARCHAR(20),
      response TEXT,
      reversal_amount INT(10),
      makeDMS_amount INT(10),
      PRIMARY KEY (id)
      );
");





$this->run("
CREATE TABLE $db_table_batch  (
      id INT(10) NOT NULL AUTO_INCREMENT,
      result TEXT,
      result_code VARCHAR(3),
      count_reversal VARCHAR(10),
      count_transaction VARCHAR(10),
      amount_reversal VARCHAR(16),
      amount_transaction VARCHAR(16),
      close_date VARCHAR(20),
      response TEXT,
      PRIMARY KEY (id)
      );
");






$this->run("
CREATE TABLE $db_table_error  (
      id INT(10) NOT NULL AUTO_INCREMENT,
      error_time VARCHAR (20),
      action VARCHAR (20),
      response TEXT,
      PRIMARY KEY (id)
      );
");

$this->endSetup();
