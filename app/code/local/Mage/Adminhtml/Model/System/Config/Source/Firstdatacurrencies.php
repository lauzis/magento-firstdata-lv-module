<?php
class Mage_Adminhtml_Model_System_Config_Source_Firstdatacurrencies
{

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        //428=LVL 978=EUR 840=USD 941=RSD 703=SKK 440=LTL 233=EEK 643=RUB 891=YUM

        return array(
            array('value' => 978, 'label'=>Mage::helper('adminhtml')->__('EUR')),
            array('value' => 840, 'label'=>Mage::helper('adminhtml')->__('USD')),
            array('value' => 941, 'label'=>Mage::helper('adminhtml')->__('RSD')),
            array('value' => 703, 'label'=>Mage::helper('adminhtml')->__('SKK')),
            array('value' => 440, 'label'=>Mage::helper('adminhtml')->__('LTL')),
            array('value' => 643, 'label'=>Mage::helper('adminhtml')->__('RUB')),
            array('value' => 891, 'label'=>Mage::helper('adminhtml')->__('YUM'))
        );
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            978 => Mage::helper('adminhtml')->__('EUR'),
            840 => Mage::helper('adminhtml')->__('USD'),
            941 => Mage::helper('adminhtml')->__('RSD'),
            703 => Mage::helper('adminhtml')->__('SKK'),
            440 => Mage::helper('adminhtml')->__('LTL'),
            643 => Mage::helper('adminhtml')->__('RUB'),
            891 => Mage::helper('adminhtml')->__('YUM')
        );
    }

}
